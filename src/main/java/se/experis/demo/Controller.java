package se.experis.demo;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    //creates an adress with the request value o input in the param.
    //a string variable for greetng will store and return a srting + the value included in greeting (input value frm index file)
    @GetMapping("/greeting")
    public String greetingPage(@RequestParam("input") String greeting){
        return "Greeting my friend, "+ greeting;
    }
    //same as above, but we use stringbuilder to make a reverse value of the string in newInput
    @GetMapping(value = "/reverse")
    public String reverse (@RequestParam("input") String newInput){

        return "The reverse for "+newInput +" is: "+ new StringBuilder(newInput).reverse().toString();
    }


}